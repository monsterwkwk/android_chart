package com.example.android_chart

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener

class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivity"
    private lateinit var pieChart: PieChart
    private var yData = arrayOf(11.5F, 12.0F, 15.8F, 14.0F)
    private var xData = arrayOf("Speaker", "Laptop", "Handphone", "Headset")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        pieChart = findViewById(R.id.cPieChart) as PieChart
        //pieChart.description.text = "PT. Indo"
        pieChart.holeRadius = 35F
        pieChart.setTransparentCircleAlpha(0)
        pieChart.centerText = "100 Penjualan"
        pieChart.setCenterTextSize(10F)
        pieChart.setDrawEntryLabels(false)

        addDataSet()

        pieChart.setOnChartValueSelectedListener(object : OnChartValueSelectedListener{
            override fun onNothingSelected() {

            }

            override fun onValueSelected(e: Entry?, h: Highlight?) {
                Toast.makeText(this@MainActivity, " Entry ${e} and Highlight ${h}", Toast.LENGTH_SHORT).show()
            }

        })
    }

    private fun addDataSet() {
        var yEntrys = ArrayList<PieEntry>()
        var xEntrys = ArrayList<String>()

        for (y in yData){
            yEntrys.add(PieEntry(y))
        }

        for (x in xData){
            xEntrys.add(x)
        }

        var pieDataSet = PieDataSet(yEntrys, "Penjualan")
        pieDataSet.sliceSpace = 2.0F
        pieDataSet.valueTextSize = 12F

        var colors = ArrayList<Int>()
        colors.add(Color.CYAN)
        colors.add(Color.RED)
        colors.add(Color.BLUE)
        colors.add(Color.GRAY)
        pieDataSet.setColors(colors)

//        var legend = pieChart.legend
//        legend.form = Legend.LegendForm.CIRCLE

        // create pie chart object
        var pieData = PieData(pieDataSet)
        pieChart.data = pieData
        pieChart.invalidate()
    }
}
